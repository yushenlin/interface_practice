<?php

namespace App\Services;
use Illuminate\Http\JsonResponse;

interface NotifyInterface
{
    /**
     * 設定標題
     *
     * @param string $title
     * @return void
     */
    public function setTitle(string $title);

    /**
     * 取得標題
     *
     * @return string
     */
    public function getTitle() : string;

    /**
     * 設定內容
     *
     * @param string $contents
     * @return void
     */
    public function setContents(string $contents);

    /**
     * 取得內容
     *
     * @return string
     */
    public function getContents() : string;

    /**
     * 寄件人
     *
     * @param string $from
     * @return void
     */
    public function from(string $from);

    /**
     * 收件人
     *
     * @param array $to
     * @return void
     */
    public function to(array $to);

    /**
     * 寄送
     *
     * @return JsonResponse
     */
    public function send() : JsonResponse;

}
