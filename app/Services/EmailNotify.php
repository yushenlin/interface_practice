<?php

namespace App\Services;
use App\Services\NotifyInterface;
use Illuminate\Http\JsonResponse;

class EmailNotify implements NotifyInterface
{
    private $title;
    private $contents;
    private $from;
    private $to;

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getTitle() : string
    {
        return $this->title;
    }

    public function setContents(string $contents)
    {
        $this->contents = $contents;
    }

    public function getContents() : string
    {
        return $this->contents;
    }

    public function from(string $from)
    {
        $this->from = $from;
    }

    public function to(array $to)
    {
        $this->to = $to;
    }

    public function send() : JsonResponse
    {
        $data = array();

        foreach ($this->to as $key => $user)
        {
            $data[$key]['title'] = $this->title;
            $data[$key]['contents'] = $this->contents;
            $data[$key]['from'] = $this->from;
            $data[$key]['to'] = $user;
        }

        return response()->json($data);
    }
}
