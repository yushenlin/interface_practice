<?php

namespace App\Http\Controllers;
use App\Services\EmailNotify;
use App\Services\LineNotify;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class NotifyController extends Controller
{

    private $emailNotify;
    private $lineNotify;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(EmailNotify $emailNotify, LineNotify $lineNotify)
    {
        $this->emailNotify = $emailNotify;
        $this->lineNotify = $lineNotify;
    }

    /**
     * 寄送郵件
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function sendMail(Request $request) : JsonResponse
    {
        $this->emailNotify->setTitle($request->title);
        $this->emailNotify->setContents($request->contents);
        $this->emailNotify->from($request->from);
        $this->emailNotify->to($request->to);
        return $this->emailNotify->send();
    }

    /**
     * 寄送 LINE 訊息
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function sendLine(Request $request) : JsonResponse
    {
        $this->lineNotify->setTitle($request->title);
        $this->lineNotify->setContents($request->contents);
        $this->lineNotify->from($request->from);
        $this->lineNotify->to($request->to);
        return $this->lineNotify->send();
    }
}
