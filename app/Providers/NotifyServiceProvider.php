<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
// use App\Http\Controllers\EmailNotifyController;
// use App\Http\Controllers\LineNotifyController;
// use App\Services\NotifyInterface;
// use App\Services\EmailNotify;
// use App\Services\LineNotify;

class NotifyServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->when(EmailNotifyController::class)
                ->needs(NotifyInterface::class)
                ->give(function () {
                    return EmailNotify::class;
                });

        $this->app->when(LineNotifyController::class)
                ->needs(NotifyInterface::class)
                ->give(function () {
                    return LineNotify::class;
                });
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.


    }
}
